from pymysql import Error

from db import connect_to_db
import datetime

from queries.queries_predmet import get_predmet
from queries.queries_student import update_student
from queries.queries_upis import insert_upis

conn, cursor = connect_to_db()


def upisi_studenta(student_id):
    nepolozeni = lista_nepolozenih(student_id)
    sljedeci = lista_sljedecih(student_id)
    preneseni_krediti = sum(predmet[3] for predmet in nepolozeni)

    print("Ovo su predmeti sa ranijih godina koje niste polozili:\n")
    stampaj_predmete(nepolozeni)
    print("Oni vrijede {} kredita. Iz liste predmeta sa sljedecih godina izaberite predmete u vrijednosti od {} "
          "kredita tako sto cete unositi njihove ID-jeve.\n".format(preneseni_krediti, 60 - preneseni_krediti))
    stampaj_predmete(sljedeci)

    while True:
        input_data = input("Ako zelite vise informacija o nekom predmetu, unesite njegov ID. Ako ne, pritisnite 'n'\n")

        if input_data == "n":
            break
        else:
            predmet = get_predmet(int(input_data))
            print(predmet[2])

    trenutni_krediti = preneseni_krediti
    izabrani_predmeti = []

    while trenutni_krediti < 60:
        predmet_id = input("ID predmeta: ")
        for predmet in sljedeci:
            if int(predmet_id) == predmet[0]:
                izabrani_predmeti.append(predmet)
                trenutni_krediti += predmet[3]
                if trenutni_krediti < 60:
                    print("Ostalo je jos {} kredita.".format(60 - trenutni_krediti))

    izabrani_predmeti.extend(nepolozeni)

    for predmet in izabrani_predmeti:
        dodaj_predmet_studentu(predmet[0], student_id)

    insert_upis(student_id, datetime.datetime.now(), 1)

    if preneseni_krediti > 15:
        prenos(student_id)


def prenos(student_id):
    update_student(student_id, 'status', 'samofinansiranje')
    update_student(student_id, 'balans', 0)


def dodaj_predmet_studentu(predmet_id, student_id):
    try:
        cursor.execute(
            """
                INSERT INTO Prijavljeni_Predmeti(predmet_id, student_id)
                VALUES (%s, %s)
            """, (predmet_id, student_id)
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def stampaj_predmete(lista_predmeta):
    print("ID | Ime predmeta | Broj kredita")
    print("--------------------------------")
    for predmet in lista_predmeta:
        print(" {} | {} | {}".format(predmet[0], predmet[1], predmet[3]))
    print("--------------------------------\n")


def lista_nepolozenih(student_id):
    try:
        cursor.execute(
            """
                SELECT *
                FROM Predmet P
                WHERE P.id NOT IN (
                    SELECT predmet_id
                    FROM Polozeni_Predmeti
                    WHERE student_id = %s
                    )
                AND P.id NOT IN (
                    SELECT predmet_id
                    FROM Prijavljeni_Predmeti
                    WHERE student_id = %s
                    )
                AND P.godina_studija <= (
                    SELECT godina_studija
                    FROM Student
                    WHERE jsk = %s
                    );
            """, (student_id, student_id, student_id)
        )
        return list(cursor.fetchall())
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def lista_sljedecih(student_id):
    try:
        cursor.execute(
            """
                SELECT *
                FROM Predmet P
                WHERE P.id NOT IN (
                    SELECT predmet_id
                    FROM Polozeni_Predmeti
                    WHERE student_id = %s
                    )
                AND P.id NOT IN (
                    SELECT predmet_id
                    FROM Prijavljeni_Predmeti
                    WHERE student_id = %s
                    )
                AND P.godina_studija = (
                    SELECT godina_studija
                    FROM Student
                    WHERE jsk = %s
                    ) + 1;
            """, (student_id, student_id, student_id)
        )
        return list(cursor.fetchall())
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))