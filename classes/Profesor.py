from queries.queries_profesor import insert_profesor, update_profesor, delete_profesor
import re


class Profesor:
    def __init__(self, ime, prezime, email):
        self.ime = ime
        self.prezime = prezime
        self.email = email

        insert_profesor(ime, prezime, email)

    def get_ime(self):
        return self.ime

    def set_ime(self, novo_ime):
        if re.match(r"^[A-Za-z\s-']+$", novo_ime):
            update_profesor(self.email, 'ime', novo_ime)
            self.ime = novo_ime
        else:
            print("Nije ispravno ime")

    def get_prezime(self):
        return self.prezime

    def set_prezime(self, novo_prezime):
        if re.match(r"^[A-Za-z\s-']+$", novo_prezime):
            update_profesor(self.email, 'prezime', novo_prezime)
            self.prezime = novo_prezime
        else:
            print("Nije ispravno prezime")

    def get_email(self):
        return self.email

    def set_email(self, novi_email):
        if re.match(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", novi_email):
            update_profesor(self.email, 'email', novi_email)
            self.email = novi_email
        else:
            print("Nije ispravan email")

    def delete(self):
        delete_profesor(self.email)
