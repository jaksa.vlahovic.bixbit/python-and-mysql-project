from queries.queries_predmet import insert_predmet, update_predmet
from queries.queries_profesor import get_profesor
import re


class Predmet:
    def __init__(self, ime, opis, broj_kredita, godina_studija, profesor_id):
        self.ime = ime,
        self.opis = opis,
        self.broj_kredita = broj_kredita,
        self.godina_studija = godina_studija,
        self.profesor_id = profesor_id

        insert_predmet(ime, opis, broj_kredita, godina_studija, profesor_id)

    def get_ime(self):
        return self.ime

    def set_ime(self, novo_ime):
        if re.match(r"^[A-Za-z\s-']+$", novo_ime):
            update_predmet(self.ime, 'ime', novo_ime)
            self.ime = novo_ime
        else:
            print("Nije ispravno ime")

    def get_opis(self):
        return self.opis

    def set_opis(self, novi_opis):
        if type(novi_opis) == str:
            update_predmet(self.ime, 'opis', novi_opis)
            self.opis = novi_opis
        else:
            print("Neispravan unos")

    def get_broj_kredita(self):
        return self.broj_kredita

    def set_broj_kredita(self, novi_broj_kredita):
        if 2 <= novi_broj_kredita <= 10 and type(novi_broj_kredita) == int:
            update_predmet(self.ime, 'broj_kredita', novi_broj_kredita)
            self.broj_kredita = novi_broj_kredita
        else:
            print("Broj kredita mora biti izmedju 2 i 10")

    def get_godina_studija(self):
        return self.godina_studija

    def set_godina_studija(self, nova_godina_studija):
        if 1 <= nova_godina_studija <= 5 and type(nova_godina_studija) == int:
            update_predmet(self.ime, 'godina_studija', nova_godina_studija)
            self.godina_studija = nova_godina_studija
        else:
            print("Godina studija mora biti izmedju 1 i 5")

    def get_profesor(self):
        return get_profesor(self.profesor_id)

    def set_profesor(self, novi_profesor_id):
        if get_profesor(novi_profesor_id):
            update_predmet(self.ime, 'profesor_id', novi_profesor_id)
            self.profesor_id = novi_profesor_id
        else:
            print("Profesor sa tim ID ne postoji")
