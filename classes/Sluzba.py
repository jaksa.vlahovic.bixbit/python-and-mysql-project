import bcrypt
import re
from queries.queries_sluzba import insert_sluzba, delete_sluzba, update_sluzba


class Sluzba:
    def __init__(self, ime, email, sifra):
        self.ime = ime
        self.email = email

        sifra_bytes = sifra.encode("utf-8")
        salt = bcrypt.gensalt()
        sifra_hash = bcrypt.hashpw(sifra_bytes, salt)

        self.sifra = sifra_hash

        insert_sluzba(self.ime, self.sifra, self.email)

    def get_ime(self):
        return self.ime

    def set_ime(self, novo_ime):
        if re.match(r"^[A-Za-z\s-']+$", novo_ime):
            update_sluzba(self.email, 'ime', novo_ime)
            self.ime = novo_ime
        else:
            print("Nije ispravno ime")

    def get_email(self):
        return self.email

    def set_email(self, novi_email):
        if re.match(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", novi_email):
            update_sluzba(self.email, 'email', novi_email)
            self.email = novi_email
        else:
            print("Nije ispravan email")

    def set_sifra(self, nova_sifra):
        sifra_bytes = nova_sifra.encode("utf-8")
        salt = bcrypt.gensalt()
        sifra_hash = bcrypt.hashpw(sifra_bytes, salt)

        update_sluzba(self.email, 'sifra', sifra_hash)
        self.sifra = sifra_hash

    def delete(self):
        delete_sluzba(self.email)