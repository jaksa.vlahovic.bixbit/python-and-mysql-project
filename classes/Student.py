from queries.queries_student import get_polozeni_predmeti, get_prijavljeni_predmeti, update_student
import re


class Student:
    def __init__(self, jsk, ime, prezime, broj_indeksa, status, godina_studija=1, balans=None):
        self.jsk = jsk
        self.ime = ime
        self.prezime = prezime
        self.broj_indeksa = broj_indeksa
        self.godina_studija = godina_studija
        self.status = status
        self.balans = balans

    def get_jsk(self):
        return self.jsk

    def get_ime(self):
        return self.ime

    def set_ime(self, novo_ime):
        if re.match(r"^[A-Za-z\s-']+$", novo_ime):
            update_student(self.jsk, 'ime', novo_ime)
            self.ime = novo_ime
        else:
            print("Nije ispravno ime")

    def get_prezime(self):
        return self.prezime

    def set_prezime(self, novo_prezime):
        if re.match(r"^[A-Za-z\s-']+$", novo_prezime):
            update_student(self.jsk, 'prezime', novo_prezime)
            self.prezime = novo_prezime
        else:
            print("Nije ispravno prezime")

    def get_broj_indeksa(self):
        return self.broj_indeksa

    def set_broj_indeksa(self, novi_broj_indeksa):
        if re.match(r"^[a-zA-Z]{2}/[a-zA-Z]{2}$", novi_broj_indeksa):
            update_student(self.jsk, 'broj_indeksa', novi_broj_indeksa)
            self.broj_indeksa = novi_broj_indeksa
        else:
            print("Nije ispravan broj indeksa")

    def get_godina_studija(self):
        return self.godina_studija

    def set_godina_studija(self, nova_godina_studija):
        if 1 <= nova_godina_studija <= 5 and type(nova_godina_studija) == int:
            update_student(self.jsk, 'godina_studija', nova_godina_studija)
            self.godina_studija = nova_godina_studija
        else:
            print("Godina studija mora biti izmedju 1 i 5")

    def get_status(self):
        return self.status

    def set_status(self, novi_status):
        if novi_status == "budzet" | novi_status == "samofinansiranje":
            update_student(self.jsk, 'status', novi_status)
            self.status = novi_status
        else:
            print("Nije ispravan status")

    def get_balans(self):
        return self.balans

    def set_balans(self, novi_balans):
        if type(novi_balans) == int:
            update_student(self.jsk, 'balans', novi_balans)
            self.balans = novi_balans
        else:
            print("Balans mora biti broj")

    def get_polozeni_predmeti(self):
        return get_polozeni_predmeti(self.jsk)

    def get_prijavljeni_predmeti(self):
        return get_prijavljeni_predmeti(self.jsk)
