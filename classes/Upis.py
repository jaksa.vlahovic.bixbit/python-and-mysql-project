from queries.queries_student import get_student
from queries.queries_upis import insert_upis, update_upis, delete_upis, get_all_upis
import datetime

class Upis:
    def __init__(self, student, skolska_godina, u_toku):
        self.skolska_godina = skolska_godina
        self.u_toku = u_toku
        self.student = student

        if get_student(student):
            insert_upis(student, skolska_godina, u_toku)
        else:
            exit(1)

    @staticmethod
    def azuriraj_skolsku_godinu():
        svi_upisi = get_all_upis()
        for upis in svi_upisi:
            upis_id = upis[0]
            update_upis(upis_id, 'skolska_godina', datetime.datetime.now())

    def get_student(self):
        return get_student(self.student)

    def get_u_toku(self):
        return self.u_toku

    def set_u_toku(self, val):
        if type(val) == bool:
            update_upis(self.student, 'u_toku', val)
            self.u_toku = val
        else:
            print("Vrijednost nije ispravna")

    def get_skolska_godina(self):
        return self.skolska_godina

    def delete(self):
        delete_upis(self.student)
