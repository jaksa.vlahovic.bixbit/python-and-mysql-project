import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# koristim komandu
# sudo python3 -m smtpd -c DebuggingServer -n localhost:1025
# radi simulacije mejl servera


def posalji_mejl(reciever='test@mail.com', file=None):
    sender_email = 'jaksa@python.com'
    reciever_email = reciever

    message = MIMEMultipart("alternative")
    message["Subject"] = "Testing mail"
    message["From"] = sender_email
    message["To"] = reciever_email

    text = "Hi, this is testing"

    message.attach(MIMEText(text, 'plain'))

    if file:
        with open(file, 'rb') as attachment:
            part = MIMEBase('application', 'octet-stream')
            part.set_payload(attachment.read())

        encoders.encode_base64(part)

        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {file}",
        )

        message.attach(part)

    with smtplib.SMTP('localhost', 1025) as server:
        server.sendmail(
            sender_email, reciever_email, message.as_string()
        )


if __name__ == "__main__":
    posalji_mejl(file="studenti_profesora_1")
