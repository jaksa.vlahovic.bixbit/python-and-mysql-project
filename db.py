import pymysql


def connect_to_db():
    conn = pymysql.connect(
        host='localhost',
        user='jaksa',
        password='Jaksa01!',
        database='python_and_mysql'
    )

    cursor = conn.cursor()

    return conn, cursor


def make_tables(cursor):
    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS Profesor (
            id INT PRIMARY KEY AUTO_INCREMENT,
            ime VARCHAR(255) NOT NULL,
            prezime VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL
        );
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS Predmet (
            id INT PRIMARY KEY AUTO_INCREMENT,
            ime VARCHAR(255) NOT NULL,
            opis VARCHAR(255),
            broj_kredita INT NOT NULL,
            godina_studija INT NOT NULL,
            profesor_id INT NOT NULL,
            FOREIGN KEY (profesor_id) REFERENCES Profesor(id)
        );
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS Student (
            jsk INT PRIMARY KEY,
            ime VARCHAR(255) NOT NULL,
            prezime VARCHAR(255) NOT NULL,
            broj_indeksa VARCHAR(5) NOT NULL,
            godina_studija INT NOT NULL,
            status VARCHAR(255) NOT NULL,
            balans INT
        );
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS Prijavljeni_Predmeti (
            predmet_id int,
            student_id int,
            PRIMARY KEY (predmet_id, student_id),
            FOREIGN KEY (predmet_id) REFERENCES Predmet(id),
            FOREIGN KEY (student_id) REFERENCES Student(jsk)
        );
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS Polozeni_Predmeti (
            predmet_id int,
            student_id int,
            PRIMARY KEY (predmet_id, student_id),
            FOREIGN KEY (predmet_id) REFERENCES Predmet(id),
            FOREIGN KEY (student_id) REFERENCES Student(jsk)
        );
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS Sluzba (
            id INT PRIMARY KEY AUTO_INCREMENT,
            ime VARCHAR(255) NOT NULL,
            sifra VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL
        );
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS Upis (
            student INT PRIMARY KEY,
            skolska_godina DATETIME NOT NULL,
            u_toku BOOL NOT NULL,
            FOREIGN KEY (student) REFERENCES Student(jsk)
        );
        """
    )


if __name__ == "__main__":
    conn, cursor = connect_to_db()
    make_tables(cursor)
