from queries.queries_sluzba import get_sluzba
from queries.queries_student import get_student
import getpass

from sluzba_edit import sluzba_edit
from sluzba_upis import sluzba_upis
from upisi_studenta import upisi_studenta

print("Koja je Vasa uloga? (student/sluzba)")

uloga = input()

if uloga == 'student':
    print("Unesite Vas jsk: ")
    jsk = input()
    data = get_student(jsk)
    if data:
        upisi_studenta(jsk)
elif uloga == 'sluzba':
    print("Unesite Vas email: ")
    email = input()
    sifra = getpass.getpass("Unesite Vasu sifru: ")
    data = get_sluzba(email, sifra)
    if data:
        print("Koja je svrha Vaseg ulogovanja? (edit/upis)")
        svrha = input()

        if svrha == 'upis':
            sluzba_upis(email)
        elif svrha == 'edit':
            sluzba_edit()
        else:
            print("Pogresan unos. Pokrenite skriptu opet i pokusajte ponovo")
else:
    print("Pogresan unos. Pokrenite skriptu opet i pokusajte ponovo")
