from pymysql import Error
from db import connect_to_db
from obavijesti_profesora import obavijesti_profesora
from posalji_mejl import posalji_mejl
import csv

conn, cursor = connect_to_db()


def svi_upisi_za_godinu(godina):
    try:
        cursor.execute(
            """
                SELECT student FROM Upis WHERE YEAR(skolska_godina)=%s;
            """, godina
        )
        return list(cursor.fetchall())
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def sluzba_upis(email):
    print("Izaberite skolsku godinu: ")
    skolska_godina = input()
    upisi = svi_upisi_za_godinu(skolska_godina)
    print(upisi)

    filename = 'upisi_za_godinu_{}'.format(skolska_godina)
    with open(filename, 'w') as f:
        write = csv.writer(f)

        write.writerow(['ime', 'prezime', 'broj_indeksa'])
        write.writerows(upisi)

    posalji_mejl(email, filename)

    print("Da li je upis zavrsen? (y/n)")
    potvrda = input()

    if potvrda == 'y':
        obavijesti_profesora()


