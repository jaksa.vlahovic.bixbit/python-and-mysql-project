from classes.Predmet import Predmet
from classes.Profesor import Profesor
from classes.Sluzba import Sluzba
from classes.Student import Student
from classes.Upis import Upis
from db import connect_to_db
from queries.queries_student import get_polozeni_predmeti, get_prijavljeni_predmeti
from sluzba_edit import sluzba_edit
from upisi_studenta import upisi_studenta

# s1 = Predmet("Matematika", "matematika", 4, 2, 3)
# s1.set_profesor(1)
# print(s1.get_profesor())
# s1.set_broj_kredita(10)

# p1 = Profesor("Jaksa", "Vlahovic", 'jaksa@mail')
# p1.delete()

# sluzba = Sluzba('jaksa', 'jaksa@mail', 'test123')

# upis = Upis(1, 2, True)
# print(upis)
# upis.set_u_toku(False)
# print(upis.get_student())

# student = Student(1, 'Jaksa', 'Vlahovic', '23/19', 4, 1)
# print(get_polozeni_predmeti(1))
# print(get_prijavljeni_predmeti(1))
#
# print(get_polozeni_predmeti(2))
# print(get_prijavljeni_predmeti(2))

# upisi_studenta(1)
sluzba_edit()

# connect_to_db()