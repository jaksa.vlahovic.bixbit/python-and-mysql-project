from queries.queries_predmet import get_predmet, insert_predmet, delete_predmet, update_predmet
from queries.queries_profesor import get_profesor, update_profesor, insert_profesor, delete_profesor
from queries.queries_student import get_student, update_student, insert_student, delete_student


def sluzba_edit():
    print("Sa kojim informacijama zelite da radite? (student/profesor/predmet)")
    tabela = input()
    if tabela == 'student':
        input_data = input("Koju akciju zelite da odradite? (GET/UPDATE/INSERT/DELETE) ")
        if input_data == "GET":
            jsk = input("Upisite JSK studenta: ")
            print(get_student(jsk))
        elif input_data == "UPDATE":
            jsk = input("Upisite JSK studenta: ")
            student = get_student(jsk)
            print(student)
            status = student[5]
            col = input("Upisite kolonu koju zelite da promijenite: ")
            val = input("Upisite novu vrijednost te kolone: ")
            if col == "balans" and status == "budzet":
                print("Ne mozete mijenjati balans studenta koji je na budzetu")
            else:
                update_student(jsk, col, val)
        elif input_data == "INSERT":
            jsk = input("Unesite JSK: ")
            ime = input("Unesite ime: ")
            prezime = input("Unesite prezime: ")
            broj_indeksa = input("Unesite broj indeks: (XX/XX) ")
            godina_studija = input("Unesite godinu studija: ")
            status = input("Unesite status: (budzet/samofinsansiranje) ")
            insert_student(jsk, ime, prezime, broj_indeksa, godina_studija, status)
        elif input_data == "DELETE":
            jsk = input("Upisite JSK studenta: ")
            delete_student(jsk)
        else:
            print("Pogresan unos")
    elif tabela == 'profesor':
        input_data = input("Koju akciju zelite da odradite? (GET/UPDATE/INSERT/DELETE) ")
        if input_data == "GET":
            profesor_id = input("Upisite ID profesora: ")
            print(get_profesor(profesor_id))
        elif input_data == "UPDATE":
            profesor_id = input("Upisite ID profesora: ")
            profesor = get_profesor(profesor_id)
            print(profesor)
            col = input("Upisite kolonu koju zelite da promijenite: ")
            val = input("Upisite novu vrijednost te kolone: ")
            email = profesor[3]
            update_profesor(email, col, val)
        elif input_data == "INSERT":
            ime = input("Unesite ime: ")
            prezime = input("Unesite prezime: ")
            email = input("Unesite email: ")
            insert_profesor(ime, prezime, email)
        elif input_data == "DELETE":
            profesor_id = input("Upisite ID profesora: ")
            profesor = get_profesor(profesor_id)
            email = profesor[3]
            delete_profesor(email)
    elif tabela == 'predmet':
        input_data = input("Koju akciju zelite da odradite? (GET/UPDATE/INSERT/DELETE) ")
        if input_data == "GET":
            predmet_id = input("Upisite ID predmeta: ")
            print(get_predmet(predmet_id))
        elif input_data == "UPDATE":
            predmet_id = input("Upisite ID predmeta: ")
            predmet = get_predmet(predmet_id)
            print(predmet)
            col = input("Upisite kolonu koju zelite da promijenite: ")
            val = input("Upisite novu vrijednost te kolone: ")
            ime = predmet[1]
            update_predmet(ime, col, val)
        elif input_data == "INSERT":
            ime = input("Unesite ime: ")
            opis = input("Unesite opis: ")
            broj_kredita = input("Unesite broj kredita: ")
            godina_studija = input("Unesite godinu studija: ")
            profesor_id = input("Unesite ID profesora: ")
            insert_predmet(ime, opis, broj_kredita, godina_studija, profesor_id)
        elif input_data == "DELETE":
            predmet_id = input("Upisite ID predmeta: ")
            predmet = get_predmet(predmet_id)
            ime = predmet[1]
            delete_predmet(ime)
    else:
        print("Pogresan unos. Pokrenite skriptu opet i pokusajte ponovo")