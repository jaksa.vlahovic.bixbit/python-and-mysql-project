from pymysql import Error
import csv
from db import connect_to_db
from posalji_mejl import posalji_mejl

conn, cursor = connect_to_db()


def svi_profesori():
    try:
        cursor.execute(
            """
                SELECT * FROM Profesor;
            """
        )
        return list(cursor.fetchall())
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def svi_predmeti_profesora(profesor_id):
    try:
        cursor.execute(
            """
                SELECT Predmet.id FROM Predmet WHERE profesor_id = %s;
            """, profesor_id
        )
        return list(cursor.fetchall())
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def svi_studenti_predmeta(predmet_id):
    try:
        cursor.execute(
            """
                SELECT ime, prezime, broj_indeksa
                FROM Prijavljeni_Predmeti, Student
                WHERE predmet_id = %s AND student_id = jsk;
            """, predmet_id
        )
        return list(cursor.fetchall())
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def obavijesti_jednog(profesor_id, profesor_email):
    predmeti_profesora = svi_predmeti_profesora(profesor_id)
    studenti_profesora = []

    for predmet_tuple in predmeti_profesora:
        predmet = predmet_tuple[0]
        studenti = svi_studenti_predmeta(predmet)
        if studenti:
            for student in studenti:
                if student not in studenti_profesora:
                    studenti_profesora.append(student)

    print(studenti_profesora)

    filename = 'studenti_profesora_{}'.format(profesor_id)
    with open(filename, 'w') as f:
        write = csv.writer(f)

        write.writerow(['ime', 'prezime', 'broj_indeksa'])
        write.writerows(studenti_profesora)

    posalji_mejl(profesor_email, filename)


def obavijesti_profesora():
    profesori = svi_profesori()

    for profesor in profesori:
        profesor_id = profesor[0]
        profesor_email = profesor[3]
        obavijesti_jednog(profesor_id, profesor_email)
