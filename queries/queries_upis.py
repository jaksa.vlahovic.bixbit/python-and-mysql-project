from pymysql import Error

from db import connect_to_db

conn, cursor = connect_to_db()


def get_all_upis():
    try:
        cursor.execute(
            """
                SELECT Upis.student FROM Upis;
            """
        )
        return list(cursor.fetchall())
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def insert_upis(student, skolska_godina, u_toku):
    try:
        cursor.execute(
            """
                INSERT INTO Upis(student, skolska_godina, u_toku) VALUES (%s, %s, %s)
            """, (student, skolska_godina, u_toku)
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def update_upis(student, col, val):
    try:
        query = "UPDATE Upis SET {} = %s WHERE student = %s".format(col)
        cursor.execute(query, (val, student))
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def delete_upis(student):
    try:
        cursor.execute(
            """
                DELETE FROM Upis WHERE student = %s
            """, student
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()
