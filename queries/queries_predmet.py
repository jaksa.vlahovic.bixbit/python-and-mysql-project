from pymysql import Error

from db import connect_to_db

conn, cursor = connect_to_db()


def insert_predmet(ime, opis, broj_kredita, godina_studija, profesor_id):
    try:
        cursor.execute(
            """
                INSERT INTO Predmet(ime, opis, broj_kredita, godina_studija, profesor_id)
                VALUES (%s, %s, %s, %s, %s)
            """, (ime, opis, broj_kredita, godina_studija, profesor_id)
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def update_predmet(ime, col, val):
    try:
        query = "UPDATE Predmet SET {} = %s WHERE ime = %s".format(col)
        cursor.execute(query, (val, ime))
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def get_predmet(predmet_id):
    try:
        cursor.execute(
            """
                SELECT * FROM Predmet WHERE Predmet.id = %s
            """, predmet_id
        )
        return cursor.fetchone()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def delete_predmet(ime):
    try:
        cursor.execute(
            """
                DELETE FROM Predmet WHERE ime = %s
            """, ime
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()
