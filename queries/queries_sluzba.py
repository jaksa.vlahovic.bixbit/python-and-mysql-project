from pymysql import Error
import bcrypt
from db import connect_to_db

conn, cursor = connect_to_db()


def insert_sluzba(ime, sifra, email):
    try:
        cursor.execute(
            """
                INSERT INTO Sluzba(ime, sifra, email)
                VALUES (%s, %s, %s)
            """, (ime, sifra, email)
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def get_sluzba(email, sifra):
    try:
        cursor.execute(
            """
                SELECT * FROM Sluzba WHERE Sluzba.email = %s
            """, email
        )
        sluzba = cursor.fetchone()

        if sluzba:
            sifra_bytes = sifra.encode("utf-8")
            sluzba_bytes = sluzba[2].encode("utf-8")
            sifra_check = bcrypt.checkpw(sifra_bytes, sluzba_bytes)
            if sifra_check:
                return sluzba
            else:
                print("Pogresna sifra")
                return None
        else:
            print('Osoba sa ovim mejlom nije registrovana u sluzbi')
            return None
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def update_sluzba(email, col, val):
    try:
        query = "UPDATE Sluzba SET {} = %s WHERE email = %s".format(col)
        cursor.execute(query, (val, email))
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def delete_sluzba(email):
    try:
        cursor.execute(
            """
                DELETE FROM Sluzba WHERE email = %s
            """, email
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()
