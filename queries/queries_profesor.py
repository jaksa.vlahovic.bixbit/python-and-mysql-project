from pymysql import Error

from db import connect_to_db

conn, cursor = connect_to_db()


def insert_profesor(ime, prezime, email):
    try:
        cursor.execute(
            """
                INSERT INTO Profesor(ime, prezime, email) VALUES (%s, %s, %s)
            """, (ime, prezime, email)
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def update_profesor(email, col, val):
    try:
        query = "UPDATE Profesor SET {} = %s WHERE email = %s".format(col)
        cursor.execute(query, (val, email))
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def get_profesor(profesor_id):
    try:
        cursor.execute(
            """
                SELECT * FROM Profesor WHERE Profesor.id = %s
            """, profesor_id
        )
        return cursor.fetchone()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def delete_profesor(email):
    try:
        cursor.execute(
            """
                DELETE FROM Profesor WHERE email = %s
            """, email
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()
