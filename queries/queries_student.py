from pymysql import Error

from db import connect_to_db

conn, cursor = connect_to_db()


def get_student(jsk):
    try:
        cursor.execute(
            """
                SELECT * FROM Student WHERE Student.jsk = %s
            """, jsk
        )
        student = cursor.fetchone()
        if student:
            return student
        else:
            print("Ne postoji student sa datim JSK-om")
            return None
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def insert_student(jsk, ime, prezime, broj_indeksa, godina_studija, status, balans=None):
    try:
        cursor.execute(
            """
                INSERT INTO
                Student(jsk, ime, prezime, broj_indeksa, godina_studija, status, balans)
                VALUES (%s, %s, %s, %s, %s, %s, %s)
            """, (jsk, ime, prezime, broj_indeksa, godina_studija, status, balans)
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def update_student(student, col, val):
    try:
        query = "UPDATE Student SET {} = %s WHERE jsk = %s".format(col)
        cursor.execute(query, (val, student))
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()


def get_polozeni_predmeti(jsk):
    try:
        cursor.execute(
            """
                SELECT ime FROM Polozeni_Predmeti, Predmet WHERE predmet_id = Predmet.id AND student_id = %s
            """, jsk
        )
        predmeti = cursor.fetchall()
        return predmeti
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def get_prijavljeni_predmeti(jsk):
    try:
        cursor.execute(
            """
                SELECT ime FROM Prijavljeni_Predmeti, Predmet WHERE predmet_id = Predmet.id AND student_id = %s
            """, jsk
        )
        predmeti = cursor.fetchall()
        return predmeti
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))


def delete_student(student):
    try:
        cursor.execute(
            """
                DELETE FROM Student WHERE jsk = %s
            """, student
        )
        conn.commit()
    except Error as err:
        print("Operacija nije uspjela.")
        print("Error: {}".format(err))
        conn.rollback()